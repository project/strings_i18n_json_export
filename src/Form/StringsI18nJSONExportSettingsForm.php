<?php

namespace Drupal\strings_i18n_json_export\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * StringsI18nJSONExportSettingsForm class.
 */
class StringsI18nJSONExportSettingsForm extends ConfigFormBase {

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a Form object.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'strings_i18n_json_export_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['strings_i18n_json_export.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('strings_i18n_json_export.settings');

    $exportTypeDefaultValue = $config->get('export_type');

    $form['settings']['export_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Export Type'),
      '#default_value' => $exportTypeDefaultValue,
      '#options' => [
        0 => $this->t('Export all strings'),
        1 => $this->t('Export only the Strings registered in the Strings Translation UI'),
      ],
    ];

    // Disable this option if user doesn't have Strings Translation UI.
    if (!$this->moduleHandler->moduleExists('string_translation_ui')) {
      $form['settings']['export_type']['#disabled'] = TRUE;
      $form['settings']['export_type']['#description'] = $this->t('To use this option you need to have the project:') . ' <a href="https://www.drupal.org/project/string_translation_ui" target="blank">' . $this->t('Strings Translation UI') . '</a>';
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $exportType = $values['export_type'];

    $config = $this->config('strings_i18n_json_export.settings');

    $config->set('export_type', $exportType);

    $config->save();

    $this->messenger()->addMessage($this->t('Saved'));
  }

}
