<?php

namespace Drupal\strings_i18n_json_export\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\locale\StringStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * StringsI18nJsonExport Service Class.
 */
class StringsI18nJsonExport {

  use StringTranslationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $config;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Storage for strings.
   *
   * @var \Drupal\locale\StringStorageInterface
   */
  private $storage;

  /**
   * Project Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $projectHandler;

  /**
   * Construct.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory, FileSystemInterface $file_system, StringStorageInterface $storage, ModuleHandlerInterface $projectHandler) {
    $this->languageManager = $language_manager;
    $this->config = $config_factory;
    $this->logger = $logger_factory->get('strings_i18n_json_export');
    $this->fileSystem = $file_system;
    $this->storage = $storage;
    $this->projectHandler = $projectHandler;
  }

  /**
   * Method to export all Json Files.
   */
  public function exportAllJsonFiles($log = NULL) {

    $languages = $this->languageManager->getLanguages();

    foreach ($languages as $lid => $language) {
      self::generateJsonFile($lid, $log);
    }
  }

  /**
   * Method to generate Json File.
   */
  public function generateJsonFile(string $lid, $log = NULL) {

    $contexts = [];

    $configStringTranslationUi = $this->config->getEditable('string_translation_ui.settings');
    $configStringsI18nJSONExport = $this->config->getEditable('strings_i18n_json_export.settings');

    if (!empty($configStringTranslationUi->get('contexts_used'))) {
      $contexts = Json::decode($configStringTranslationUi->get('contexts_used'));
      sort($contexts);
    }

    $exportType = $configStringsI18nJSONExport->get('export_type');

    if ($this->projectHandler->moduleExists('string_translation_ui') && $exportType == 1) {
      $json = $this->getTranslationsJson($lid, $contexts);
    }
    else {
      $json = $this->getAllTranslationsJson($lid);
    }

    if (empty($json)) {

      $this->logger->error($this->t('Error on export file for language: @language', [
        '@language' => $lid,
      ]));

      return FALSE;
    }

    return $this->exportJsonFile($json, $lid, $log);
  }

  /**
   * Method to export JSON file.
   */
  protected function exportJsonFile($json, $lid, $log = NULL) {

    // Get public folder.
    $publicPath = PublicStream::basePath();
    $path = $this->fileSystem->realpath($publicPath);
    $path .= '/lang';

    // Create directory.
    $directory = \Drupal::service('file_system')->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);

    // Check if directory was created.
    if (empty($directory)) {

      $this->logger->error($this->t('Error creating folder: @folder', [
        '@folder' => $path,
      ]));

      return FALSE;
    }

    $fileName = $lid . '.json';
    $fullPath = $path . '/' . $fileName;

    // Delete old file.
    if (file_exists($fullPath)) {
      unlink($fullPath);
    }

    // Save the new file.
    $saved = file_put_contents($fullPath, $json);

    if (!$saved) {
      $this->logger->error($this->t('Error on export file'));
      return FALSE;
    }

    if ($log) {
      $this->logger->info($this->t('File i18n for language: @language has been exported. Path: @path', [
        '@language' => $lid,
        '@path' => $fullPath,
      ]));
    }

    // Return file path.
    return $fullPath;
  }

  /**
   * Method to get All Translations JSON.
   */
  public function getAllTranslationsJson(string $lid) {

    $strings = $this->storage->getTranslations(['language' => $lid]);

    if (empty($strings)) {
      return FALSE;
    }

    $source = '';
    $translation = '';

    foreach ($strings as $string) {

      $source = $string->source;
      $translation = $string->translation;
      $context = 'default';

      if (!empty($string->context)) {
        $context = $string->context;
      }

      if (empty($translation)) {
        $translation = $source;
      }

      $data[$context][$source] = $translation;
    }

    $json = Json::encode($data, JSON_HEX_AMP);

    return $json;
  }

  /**
   * Method to get JSON.
   */
  public function getTranslationsJson(string $lid, array $contexts = []) {

    $data = [];
    $strings = '';

    foreach ($contexts as $context) {

      $data[$context] = [];

      $strings = $this->storage->getTranslations(['context' => $context, 'language' => $lid]);

      $source = '';
      $translation = '';

      foreach ($strings as $string) {

        $source = $string->source;
        $translation = $string->translation;

        if (empty($translation)) {
          $translation = $source;
        }

        $data[$context][$source] = $translation;
      }
    }

    $json = Json::encode($data, JSON_HEX_AMP);

    return $json;
  }

}
